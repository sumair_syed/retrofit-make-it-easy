package com.example.vozye.gettingstartedwithretrofit.PostApiService;

/**
 * Created by Vozye on 1/12/2017.
 */

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClientPost {

    public static final String BASE_URL = "http://developer.mothers.pk";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        //httpClient.connectTimeout(1, TimeUnit.MINUTES);
        httpClient.retryOnConnectionFailure(true);

        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}