package com.example.vozye.gettingstartedwithretrofit.GetApiService;

/**
 * Created by Vozye on 1/12/2017.
 */

import com.example.vozye.gettingstartedwithretrofit.Students.BaseStudent;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface ApiInterface {
    @GET("/shariq/tutor-finder/api/data/all")
    Call<BaseStudent> getAllData();

    @POST("api/user/register")
    Call<BaseStudent> registerUser(@Field("fullName") String fullName,
                                   @Field("email") String email,
                                   @Field("password") String password,
                                   @Field("type") int type,
                                   @Field("device_id") String device_id);
}