
package com.example.vozye.gettingstartedwithretrofit.Models.StoreResultModel.FileResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FileUploadResponse {

    @SerializedName("ErrorCode")
    @Expose
    private String errorCode;
    @SerializedName("ErrorDescription")
    @Expose
    private String errorDescription;
    @SerializedName("FileName")
    @Expose
    private String fileName;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

}
