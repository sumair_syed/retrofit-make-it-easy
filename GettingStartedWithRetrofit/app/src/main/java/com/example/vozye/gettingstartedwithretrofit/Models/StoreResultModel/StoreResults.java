
package com.example.vozye.gettingstartedwithretrofit.Models.StoreResultModel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StoreResults {

    @SerializedName("ErrorCode")
    @Expose
    private String errorCode;
    @SerializedName("ErrorDescription")
    @Expose
    private String errorDescription;
    @SerializedName("TotalRecords")
    @Expose
    private Integer totalRecords;
    @SerializedName("CurrentPage")
    @Expose
    private Integer currentPage;
    @SerializedName("ShowingRecords")
    @Expose
    private Integer showingRecords;
    @SerializedName("Items")
    @Expose
    private List<Item> items = null;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getShowingRecords() {
        return showingRecords;
    }

    public void setShowingRecords(Integer showingRecords) {
        this.showingRecords = showingRecords;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
