package com.example.vozye.gettingstartedwithretrofit.PostApiService;

import com.example.vozye.gettingstartedwithretrofit.Models.StoreResultModel.FileResponse.FileUploadResponse;
import com.example.vozye.gettingstartedwithretrofit.Models.StoreResultModel.StoreResults;
import com.example.vozye.gettingstartedwithretrofit.Students.BaseStudent;

import java.io.File;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Vozye on 1/12/2017.
 */

public interface ApiInterfacePost {

    @FormUrlEncoded
    @POST("/mobile/store/find_by_category")
    Call<StoreResults> registerUser(@Field("category_id") String category_id,
                                    @Field("curr_page") String  curr_page,
                                    @Field("loginId") String loginId);
    @Multipart
    @POST("/mobile/accounts.ashx?upload_profile_pic")
    Call<FileUploadResponse> fileUpload(@Part MultipartBody.Part file);
        }
