package com.example.vozye.gettingstartedwithretrofit;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.vozye.gettingstartedwithretrofit.GetApiService.ApiClient;
import com.example.vozye.gettingstartedwithretrofit.GetApiService.ApiInterface;
import com.example.vozye.gettingstartedwithretrofit.Models.StoreResultModel.FileResponse.FileUploadResponse;
import com.example.vozye.gettingstartedwithretrofit.Models.StoreResultModel.Item;
import com.example.vozye.gettingstartedwithretrofit.Models.StoreResultModel.StoreResults;
import com.example.vozye.gettingstartedwithretrofit.PostApiService.ApiClientPost;
import com.example.vozye.gettingstartedwithretrofit.PostApiService.ApiInterfacePost;
import com.example.vozye.gettingstartedwithretrofit.Students.BaseStudent;
import com.example.vozye.gettingstartedwithretrofit.Students.Country;
import com.example.vozye.gettingstartedwithretrofit.Students.Day;
import com.example.vozye.gettingstartedwithretrofit.Students.Grade;
import com.example.vozye.gettingstartedwithretrofit.Students.Qualification;
import com.example.vozye.gettingstartedwithretrofit.Students.Subject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    List<Country> countries;
    List<Day> days;
    List<Grade> grades;
    List<Qualification> qualification;
    List<Subject> subjects;
    List<Item> itemsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        ApiInterface service = ApiClient.getClient().create(ApiInterface.class);
//        final Call<BaseStudent> baseStudent =  service.getAllData();
//        baseStudent.enqueue(new Callback<BaseStudent>() {
//            @Override
//            public void onResponse(Call<BaseStudent> call, Response<BaseStudent> response) {
//                countries = response.body().getCountries();
//                days = response.body().getDays();
//                grades = response.body().getGrades();
//                qualification =  response.body().getQualifications();
//                subjects = response.body().getSubjects();
//            }
//
//            @Override
//            public void onFailure(Call<BaseStudent> call, Throwable t) {
//                Toast.makeText(MainActivity.this,t.toString(),Toast.LENGTH_LONG).show();
//            }
//        });


        File fileDirectory = new File(Environment.getExternalStorageDirectory() + "/DCIM/Screenshots/1.png");
        if(fileDirectory.exists()) {

            ApiInterfacePost service = ApiClientPost.getClient().create(ApiInterfacePost.class);


            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileDirectory);
            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part body = MultipartBody.Part.createFormData("File", fileDirectory.getName(), requestFile);



            final Call<FileUploadResponse> fileUploadCall = service.fileUpload(body);
            fileUploadCall.enqueue(new Callback<FileUploadResponse>() {
                @Override
                public void onResponse(Call<FileUploadResponse> call, Response<FileUploadResponse> response) {
                    Toast.makeText(MainActivity.this, response.body().getFileName(), Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(Call<FileUploadResponse> call, Throwable t) {
                    Toast.makeText(MainActivity.this, t.toString(), Toast.LENGTH_LONG).show();

                }
            });
        }else
            Toast.makeText(this, "File Not Found", Toast.LENGTH_SHORT).show();
        /*final Call<StoreResults> storeResultsCall =  service.registerUser("105","0","0");
        storeResultsCall.enqueue(new Callback<StoreResults>() {
            @Override
            public void onResponse(Call<StoreResults> call, Response<StoreResults> response) {
                itemsList = response.body().getItems();
                itemsList.size();
            }

            @Override
            public void onFailure(Call<StoreResults> call, Throwable t) {
                Toast.makeText(MainActivity.this,t.toString(),Toast.LENGTH_LONG).show();
            }
        });*/


    }
}
