
package com.example.vozye.gettingstartedwithretrofit.Students;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseStudent {

    @SerializedName("countries")
    @Expose
    private List<Country> countries = null;
    @SerializedName("days")
    @Expose
    private List<Day> days = null;
    @SerializedName("qualifications")
    @Expose
    private List<Qualification> qualifications = null;
    @SerializedName("subjects")
    @Expose
    private List<Subject> subjects = null;
    @SerializedName("grades")
    @Expose
    private List<Grade> grades = null;
    @SerializedName("defaultCurrency")
    @Expose
    private String defaultCurrency;

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    public List<Day> getDays() {
        return days;
    }

    public void setDays(List<Day> days) {
        this.days = days;
    }

    public List<Qualification> getQualifications() {
        return qualifications;
    }

    public void setQualifications(List<Qualification> qualifications) {
        this.qualifications = qualifications;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public List<Grade> getGrades() {
        return grades;
    }

    public void setGrades(List<Grade> grades) {
        this.grades = grades;
    }

    public String getDefaultCurrency() {
        return defaultCurrency;
    }

    public void setDefaultCurrency(String defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
    }

}
